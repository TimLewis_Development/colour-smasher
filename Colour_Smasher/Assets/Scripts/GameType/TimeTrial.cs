﻿using UnityEngine;
using System.Collections;

namespace GameType
{
    public class TimeTrial : Base 
    {
        public float maxTime = 5;
        private float startTime;

        public override void Init()
        {
            base.Init();

            startTime = Time.time;
            battleBar.InitScores(maxTime, 1.0f);
        }

        public override void DoUpdate()
        {
            float elapsedTime = Time.time - startTime;
            float percentageComplete = 1.0f - (elapsedTime / maxTime);

            battleBar.UpdateOverlayBar(percentageComplete);
        }

        public override bool IsGameOver()
        {
            if(Time.time - startTime >= maxTime)
            {
                battleBar.UpdateOverlayBar(1.0f);

                return true;
            }

            return false;
        }

        public override void DoGameOver()
        {
        }

        public override void ProcessAnswer(int _playerID, bool _result, float _answerTime)
        {
        }
    }
}