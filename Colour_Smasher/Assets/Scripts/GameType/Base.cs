﻿using UnityEngine;
using System.Collections;

namespace GameType
{
    public enum GameMode
    {
        infinite,
        timeTrial,
        battle
    }

    public abstract class Base : MonoBehaviour 
    {
        protected GameManager manager;
        protected BattleBar_Controller battleBar;

        public virtual void Init()
        {
            if(gameObject.GetComponent<GameManager>() != null)
            {
                manager = gameObject.GetComponent<GameManager>();
            }

            if(manager.battleBarCanvas.GetComponent<BattleBar_Controller>() != null)
            {
                battleBar = manager.battleBarCanvas.GetComponent<BattleBar_Controller>();
            }
        }

        public abstract void DoUpdate();
        public abstract bool IsGameOver();
        public abstract void DoGameOver();
        public abstract void ProcessAnswer(int _playerID, bool _result, float _answerTime);
    }
}