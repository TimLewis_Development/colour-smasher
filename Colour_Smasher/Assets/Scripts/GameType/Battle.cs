﻿using UnityEngine;
using System.Collections;

namespace GameType
{
    public class Battle : Base 
    {
        private float totalPoints = 1000;

        public override void Init()
        {
            base.Init();

            battleBar.InitScores(totalPoints, 0.5f);
        }

        public override void DoUpdate()
        {
        }

        public override bool IsGameOver()
        {
            for(int i = 0; i < manager.playerInfo.Count; i++)
            {
                if(manager.playerInfo[i].score <= 0)
                {
                    return true;
                }
            }

            return false;
        }

        public override void DoGameOver()
        {
        }

        public override void ProcessAnswer(int _playerID, bool _result, float _answerTime)
        {
            for(int i = 0; i < manager.playerInfo.Count; i++)
            {
                manager.playerInfo[i].score += i == _playerID ? 20 : -20;
            }

            UpdateBattleBar();
        }

        private void UpdateBattleBar()
        {
            if(battleBar != null)
            {
                battleBar.UpdateOverlayBar(manager.playerInfo[1].score / totalPoints);
            }
        }
    }
}